import XCTest

import worksheetTests

var tests = [XCTestCaseEntry]()
tests += worksheetTests.allTests()
XCTMain(tests)
