
# Worksheet

A Swift tool for generating LaTeX math worksheets from JSON descriptor files.

## Usage

```
worksheet [-o output_file] input_file
```

## Input File Format Example

```
{
"sections" : [
    {
    "title" : "Section 1 - Addition.",
    "count" : 10,
    "operation" : "addition",
    "operandCount" : 3,
    "minValue" : 1,
    "maxValue" : 30,
    },
    
    {
    "title" : "Section 2 - Multiplication and Division.",
    "count" : 10,
    "operations" : ["multiplication", "division"],
    "operandCount" : 2,
    "minDigits" : 1,
    "maxDigits" : 3
    }
]
}
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
