//
//  SeededRandomNumberGenerator.swift
//  worksheet
//
//  Created by Philip White on 9/23/19.
//  Copyright © 2019 Philip White. All rights reserved.
//

import Foundation
import GameKit

class SeededRandomNumberGenerator : RandomNumberGenerator {
    
    let randomSource : GKARC4RandomSource
    
    init(seed : String) {
        randomSource = GKARC4RandomSource(seed: seed.data(using: .utf8)!)
    }
    
    func next<T>() -> T where T : FixedWidthInteger, T : UnsignedInteger {
        return T(abs(randomSource.nextInt()))
    }
}
