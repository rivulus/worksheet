//
//  Descriptor.swift
//  worksheet
//
//  Created by Philip White on 9/23/19.
//

import Foundation

struct Descriptor : Codable {
    let sections : [Section]
}

struct Section : Codable {
    let title : String
    let count : Int
    let operation : String?
    let operations : [String]?
    let vertical : Bool?
    let operandCount : Int
    let fixedOperand : Int?
    let minValue : Int?
    let maxValue : Int?
    let minDigits : Int?
    let maxDigits : Int?
}
