//
//  latex.swift
//  worksheet
//
//  Created by Philip White on 9/23/19.
//

import Foundation

func latexForDescriptor(_ descriptor : Descriptor, seed : String) -> String {
    var output = ""
    
    output.append(contentsOf: """
\\documentclass[leqno]{article}
\\usepackage[papersize={8.5in,11in},top=0.5in,bottom=0.5in,left=0.25in,right=0.25in]{geometry}

\\usepackage{mathtools}

\\begin{document}
""")
    
    var generator = SeededRandomNumberGenerator(seed: seed)
    
    for section in descriptor.sections {
        output.append("\n\\vskip 0.25in \\noindent \\large \(section.title)\\\\\n")
        output.append("\\normalsize\n")
        output.append("\\noindent\n")
        
        for n in 0 ..< section.count {
            let op = chooseOperator(section, using: &generator)
            
            var a : Int = 0
            var b : Int = 0
            
            chooseOperands(section, &a, &b, using: &generator)
            
            if section.vertical != nil && section.vertical! == true {
                
                let tabular = tabularForVerticalEquation(a: a, b: b, oper: op)
                output.append("\\parbox{0.1\\textwidth}{\(tabular)}\n")
                output.append("\\hskip 0.233\\textwidth\n")
                
            } else {
                output.append("\\parbox{0.1\\textwidth}{\\flushright \\(\(a)\(op)\(b)=\\)}\n")
                output.append("\\hskip 0.233\\textwidth\n")
            }
            if n % 3 == 2 { // last column
                output.append("\\\\\n")
            }
        }
    }
    
    output.append(contentsOf: "\\end{document}\n")
    return output
}
    
func chooseOperator<T>(_ section : Section, using generator: inout T) -> String where T : RandomNumberGenerator{
    var operation : String?
    operation = section.operation
    
    if operation == nil {
        operation = section.operations![Int.random(in: 0 ..< section.operations!.count, using:&generator)]
    }
    
    switch operation {
    case "multiplication":
        return "\\times"
    case "addition":
        return "+"
    case "division":
        return "\\divide"
    case "subtraction":
        return "-"
    default:
        return "?"
    }
}

func chooseOperands<T>(_ section : Section, _ a : inout Int, _ b : inout Int, using generator: inout T) where T : RandomNumberGenerator {
    
    var aValueRange : ClosedRange<Int>!
    var bValueRange : ClosedRange<Int>!
    
    if let minDigits = section.minDigits, let maxDigits = section.maxDigits {
        let aNumDigits = Int.random(in: minDigits ... maxDigits, using: &generator)
        let bNumDigits = Int.random(in: minDigits ... maxDigits, using: &generator)
        aValueRange = Int(pow(10.0,Double(aNumDigits - 1))) ... Int(pow(10.0,Double(aNumDigits)) - 1)
        bValueRange = Int(pow(10.0,Double(bNumDigits - 1))) ... Int(pow(10.0,Double(bNumDigits)) - 1)
        
        
    } else {
        // linear distribution betwen minValue and maxValue
        aValueRange = section.minValue! ... section.maxValue!
        bValueRange = aValueRange
    }
    
    if let fixedOperand = section.fixedOperand {
        a = fixedOperand
        b = Int.random(in: bValueRange, using: &generator)
        if Bool.random(using: &generator) == true {
            swap(&a, &b)
        }
    } else {
        a = Int.random(in: aValueRange, using: &generator)
        b = Int.random(in: bValueRange, using: &generator)
    }
}
    
    
func decimalDigits(_ input : Int) -> [Int] {
    var digits = [Int]()
    
    var i = input
    
    while i > 0 {
        digits.insert(i % 10, at: 0)
        i = i/10
    }
    return digits
}

func tabularForVerticalEquation(a : Int, b : Int, oper : String) -> String {
    
/*
\begin{tabular}{c@{\,}c@{\,}c@{\,}c}
& 1 & 2 & 3 \\
+ &   & 3 & 4 \\
\hline
& 1 & 5 & 7 \\
\end{tabular}
*/
    var top = a
    var bottom = b
    if top < bottom {
        swap(&top,&bottom)
    }
    let topDigits = decimalDigits(top)
    let bottomDigits = decimalDigits(bottom)
    
    let columnCount = topDigits.count + 1 //plus one for operator
    var output = """
    \\vskip 0.25in
    \\begin{tabular}{c
    """
    
    for _ in 0 ..< columnCount {
        output.append(contentsOf: "@{\\,}c")
    }
    output.append(contentsOf: "}\n")
    for digit in topDigits {
        output.append(" & \(digit)")
    }
    output.append("\\\\\n")
    output.append("\\(\(oper)\\)")
    // empty places, if any
    for _ in 0 ..< (topDigits.count-bottomDigits.count) {
        output.append(" & ")
    }
    for digit in bottomDigits {
        output.append(" & \(digit)")
    }
    output.append("""
\\\\
\\hline
\\end{tabular}
\\vskip 1in
""")
    
    return output
}

