import Foundation

struct worksheet {
    var text = "Hello, World!"
}

let arguments = Array(ProcessInfo.processInfo.arguments.dropFirst())

var inputFileName = arguments.first
if inputFileName == nil {
    //inputFileName = "descriptor.json"
    inputFileName = "/src/worksheet/descriptor.json"
}

let jsonDecoder = JSONDecoder()

let data = try! Data(contentsOf: URL(fileURLWithPath:inputFileName!))

let descriptor = try! jsonDecoder.decode(Descriptor.self, from: data)

let latex = latexForDescriptor(descriptor, seed: "Philip")

try! latex.write(to: URL(fileURLWithPath:"/src/worksheet/latex/output.tex"), atomically: true, encoding: .utf8)

print(latex)
